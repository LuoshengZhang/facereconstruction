﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System;
using UnityEngine.UI;

using System.Runtime.InteropServices;

public class HandlerOperation : MonoBehaviour {

    public GameObject currentObject;
    public Mesh currentMesh;
    private GameObject cloneObject;
    private Mesh cloneMesh;

    public Material lineMaterial;
    private Vector3 minOfAABB;
    private Vector3 maxOfAABB;

    //save all handles
    private List<int> handleIndices;
    private List<Vector3> handlePositions;
    public GameObject prefabSphere;
    private List<GameObject> handles;
    private List<bool> handleState;

    //line render
    private GameObject axisXRendererObject;
    private GameObject axisYRendererObject;
    private GameObject axisZRendererObject;
    private LineRenderer axisXRenderer;
    private LineRenderer axisYRenderer;
    private LineRenderer axisZRenderer;

    //sliders
    public Slider xSlider;
    public Slider ySlider;
    public Slider zSlider;

    //VertexSelection
    public VertexSelection vertexSeletionObject;

    [DllImport("LaplacianMeshEditingPlugin")]
    public static extern
    void editing(Vector3[] vertices, int verticeSize, int[] faces, int faceSize,
        int[] fixPointsIndices, int fixPointsSize,
        int[] handlePointsIndices, Vector3[] handleNewPoints, int handlePointSize);

    // Use this for initialization
    void Start () {
        currentObject = GameObject.Find("/bunny_sim/default/");
        if(currentObject)
        {
            currentMesh = currentObject.GetComponent<MeshFilter>().mesh;
            MeshCollider collider = currentObject.GetComponent<MeshCollider>();

            if(!collider)
            {
                currentObject.AddComponent<MeshCollider>();
            }

            calculateMeshAABB();
            cloneObject = Instantiate(currentObject) as GameObject;
            cloneObject.SetActive(false);
            cloneMesh = cloneObject.GetComponent<MeshFilter>().mesh;
        }

        handles = new List<GameObject>();
        handleIndices = new List<int>();
        handleState = new List<bool>();
        handlePositions = new List<Vector3>();

        axisXRenderer = GameObject.Find("/AxisXLineRendererObject").GetComponent<LineRenderer>();
        axisXRenderer.SetWidth(2.0f, 2.0f);

        axisYRenderer = GameObject.Find("/AxisYLineRendererObject").GetComponent<LineRenderer>();
        axisYRenderer.SetWidth(2.0f, 2.0f);

        axisZRenderer = GameObject.Find("/AxisZLineRendererObject").GetComponent<LineRenderer>();
        axisZRenderer.SetWidth(2.0f, 2.0f);

        vertexSeletionObject = GameObject.Find("/MainCamera").GetComponent<VertexSelection>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))//left button
        {
            if (Input.GetKey(KeyCode.A))//add Handle
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (currentObject == hit.collider.gameObject)
                    {
                        clearHandleState();

                        GameObject handle = Instantiate(prefabSphere) as GameObject;
                        handle.name = "handle_" + handles.Count.ToString();

                        int closestVertexIndex = findClosestVertexOnMesh(hit.point);
                        handle.transform.position = currentMesh.vertices[closestVertexIndex];

                        handles.Add(handle);
                        handleIndices.Add(closestVertexIndex);
                        handleState.Add(true);
                        handlePositions.Add(currentMesh.vertices[closestVertexIndex]);
                    }
                }
            }
        }

        //select one handle
        if (Input.GetMouseButtonDown(0))//left button
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                clearHandleState();
                for (int i = 0; i < handles.Count; i++)
                {
                    if (hit.collider.gameObject == handles[i])
                    {
                        handleState[i] = true;
                        break;
                    }
                }
            }
        }

        //delete one handle
        if (Input.GetMouseButtonDown(0))//left button
        {
            if (Input.GetKey(KeyCode.LeftShift))//add Handle
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    clearHandleState();
                    for (int i = 0; i < handles.Count; i++)
                    {
                        if (hit.collider.gameObject == handles[i])
                        {
                            Destroy(handles[i]);
                            handles.RemoveAt(i);
                            handleIndices.RemoveAt(i);
                            handlePositions.RemoveAt(i);
                            handleState.RemoveAt(i);

                            break;
                        }
                    }
                }
            }
        }
    }

    void OnPostRender()
    {
        for (int i = 0; i < handleState.Count; i++)
        {
            if (handleState[i])
            {
                Vector3 origin = handles[i].transform.position;
                Vector3 axisX = origin + new Vector3(1.0f, 0.0f, 0.0f) * 20;
                Vector3 axisY = origin + new Vector3(0.0f, 1.0f, 0.0f) * 20;
                Vector3 axisZ = origin + new Vector3(0.0f, 0.0f, 1.0f) * 20;
                Vector3 axisX_ = origin - new Vector3(1.0f, 0.0f, 0.0f) * 20;
                Vector3 axisY_ = origin - new Vector3(0.0f, 1.0f, 0.0f) * 20;
                Vector3 axisZ_ = origin - new Vector3(0.0f, 0.0f, 1.0f) * 20;

                axisXRenderer.SetPosition(0, axisX_);
                axisXRenderer.SetPosition(1, axisX);

                axisYRenderer.SetPosition(0, axisY_);
                axisYRenderer.SetPosition(1, axisY);

                axisZRenderer.SetPosition(0, axisZ_);
                axisZRenderer.SetPosition(1, axisZ);
            }
        }
    }

    public void OnSliderChange()
    {
        if(currentMesh && cloneMesh)
        {
            Vector3 MovDir = maxOfAABB - minOfAABB;
            MovDir.x *= xSlider.value;
            MovDir.y *= ySlider.value;
            MovDir.z *= zSlider.value;

            for (int i = 0; i < handleState.Count; i++)
            {
                if(handleState[i])
                {
                    Vector3[] vertices = currentMesh.vertices;
                    vertices[handleIndices[i]] = cloneMesh.vertices[handleIndices[i]] + MovDir;

                    handles[i].transform.position = vertices[handleIndices[i]];
                    handlePositions[i] = vertices[handleIndices[i]];

                    //拉普拉斯变形
                    Vector3[] newVertices = cloneMesh.vertices;
                    int[] faces = cloneMesh.triangles;
                    editing(newVertices, newVertices.Length, faces, faces.Length / 3,
                        vertexSeletionObject.fixedVerticesIdx.ToArray(), vertexSeletionObject.fixedVerticesIdx.Count,
                        handleIndices.ToArray(), handlePositions.ToArray(), handleIndices.Count);

                    currentMesh.vertices = newVertices;
                    currentMesh.RecalculateNormals();
                    currentMesh.UploadMeshData(false);

                    break;
                }
            }
        }
    }

    //private functions
    int findClosestVertexOnMesh(Vector3 refVertex)
    {
        int closestIndex = 0;
        double closestDist = 100000000000;

        for(int i = 0; i < currentMesh.vertexCount; i++)
        {
            double dist = getEuclideanDistance(currentMesh.vertices[i], refVertex);
            if(dist < closestDist)
            {
                closestDist = dist;
                closestIndex = i;
            }
        }

        return closestIndex;
    }

    double getEuclideanDistance(Vector3 v0, Vector3 v1)
    {
        double D = (v0.x - v1.x) * (v0.x - v1.x);
        D += (v0.y - v1.y) * (v0.y - v1.y);
        D += (v0.z - v1.z) * (v0.z - v1.z);

        return Math.Sqrt(D);
    }

    void clearHandleState()
    {
        for(int i = 0; i < handleState.Count; i++)
        {
            handleState[i] = false;
        }
    }

    void calculateMeshAABB()
    {
        if(currentMesh)
        {
            minOfAABB = new Vector3(100000000.0f, 100000000.0f, 100000000.0f);
            maxOfAABB = new Vector3(-100000000.0f, -100000000.0f, -100000000.0f);

            for(int i = 0; i < currentMesh.vertexCount; i++)
            {
                //min
                if(currentMesh.vertices[i].x < minOfAABB.x)
                {
                    minOfAABB.x = currentMesh.vertices[i].x;
                }
                if (currentMesh.vertices[i].y < minOfAABB.y)
                {
                    minOfAABB.y = currentMesh.vertices[i].y;
                }
                if (currentMesh.vertices[i].z < minOfAABB.z)
                {
                    minOfAABB.z = currentMesh.vertices[i].z;
                }

                //max
                if (currentMesh.vertices[i].x > maxOfAABB.x)
                {
                    maxOfAABB.x = currentMesh.vertices[i].x;
                }
                if (currentMesh.vertices[i].y > maxOfAABB.y)
                {
                    maxOfAABB.y = currentMesh.vertices[i].y;
                }
                if (currentMesh.vertices[i].z > maxOfAABB.z)
                {
                    maxOfAABB.z = currentMesh.vertices[i].z;
                }

            }
        }
    }

    public void resetCurentObject()
    {
        if (cloneMesh && currentMesh)
        {
            currentMesh.vertices = cloneMesh.vertices;
            currentMesh.RecalculateNormals();
            currentMesh.UploadMeshData(false);
        }
    }

    public void saveCurrentObject()
    {
        if (cloneMesh)
        {
            FileOperation.saveMesh(cloneMesh.vertices, cloneMesh.triangles, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\mesh.txt");
        }
    }

    public void saveHandlePoints()
    {
        FileOperation.saveHandlePoints(handleIndices, handlePositions, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\handle.txt");
    }
}
