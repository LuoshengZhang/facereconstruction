﻿using UnityEngine;
using System.Collections;

public class FreeView : MonoBehaviour {

    private enum MouseButton
    {
        //鼠标左键  
        MouseButton_Left = 0,
        //鼠标右键  
        MouseButton_Right = 1,
        //鼠标中键  
        MouseButton_Middle = 2
    }

    //平移
    private Vector3 PreMouseMPos;
    private Vector3 PreMouseLPos;
    private float translateSpeed = 5f;

    //缩放
    //鼠标缩放速率  
    private float scaleSpeed = 200f;

    //旋转
    //旋转速度  
    private float rotateSpeed = 2f;
    //角度限制  
    private float MinLimitY = -60f;
    private float MaxLimitY = 60f;
    //旋转角度  
    private float rotationY = 0.0f;
    //存储角度的四元数  
    private Quaternion mRotation;

    void Update()
    {
        //鼠标中键按下，上下左右移动相机 
        translate();
        //滚轮前进后退  
        scale();
        //右键旋转视角  
        rotate();
    }

    private void translate()
    {
        //鼠标中键按下，上下左右移动相机  
        if (Input.GetMouseButton((int)MouseButton.MouseButton_Middle))
        {
            if (PreMouseMPos.x <= 0)
            {
                PreMouseMPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f);
            }
            else
            {
                Vector3 CurMouseMPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f);
                Vector3 offset = CurMouseMPos - PreMouseMPos;
                offset = - offset * 0.1f;//0.1这个数字的大小可以调节速度  
                transform.Translate(offset * translateSpeed);
                PreMouseMPos = CurMouseMPos;
            }
        }
        else
        {
            PreMouseMPos = new Vector3(0.0f, 0.0f, 0.0f);
        }
    }

    //滚轮前进后退  
    private void scale()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            gameObject.transform.Translate(new Vector3(0, 0, Input.GetAxis("Mouse ScrollWheel") * scaleSpeed));
        }
    }

    //右键旋转视角  
    private void rotate()
    {
        if (Input.GetMouseButton((int)MouseButton.MouseButton_Right))
        {
            ////获取鼠标输入  
            //rotationX -= Input.GetAxis("Mouse X") * rotateSpeed;
            //rotationY += Input.GetAxis("Mouse Y") * rotateSpeed;
            ////范围限制  
            //rotationY = ClampAngle(rotationY, MinLimitY, MaxLimitY);
            //mRotation = Quaternion.Euler(rotationY, -rotationX, 0);
            //transform.rotation = mRotation;


            //根据鼠标移动的快慢(增量), 获得相机左右旋转的角度(处理X)  
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * rotateSpeed;
            //根据鼠标移动的快慢(增量), 获得相机上下旋转的角度(处理Y)  
            rotationY += Input.GetAxis("Mouse Y") * rotateSpeed;
            //角度限制. rotationY小于min,返回min. 大于max,返回max. 否则返回value  
            rotationY = Mathf.Clamp(rotationY, MinLimitY, MaxLimitY);

            //总体设置一下相机角度  
            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
    }

    //角度限制  
    //private float ClampAngle(float angle, float min, float max)
    //{
    //    if (angle < -360) angle += 360;
    //    if (angle > 360) angle -= 360;
    //    return Mathf.Clamp(angle, min, max);
    //}

}
